import Image1 from '../../assets/image/ride/feature-cafe.svg';
import Image2 from '../../assets/image/ride/feature-chicken.svg';
import Image3 from '../../assets/image/ride/feature-basket.svg';
import Image4 from '../../assets/image/ride/009-apples.svg';
import Image5 from '../../assets/image/ride/006-market.svg';
import Image6 from '../../assets/image/ride/feature-fish.svg';
import Latest1 from '../../assets/image/ride/blog_1.jpg';
import Latest2 from '../../assets/image/ride/blog_2.jpg';
import Client1 from '../../assets/image/saas/testimonial/client-1.jpg';
import Client2 from '../../assets/image/agency/client/denny.png';

export const MENU_ITEMS = [
  {
    label: 'Home',
    path: '#banner_section',
    offset: '70',
  },
   {
    label: 'Our Services',
    path: '#feature_section',
    offset: '70',
  },

  {
    label: 'About Us',
    path: '#fare_section',
    offset: '70',
  },
 
  {
    label: 'Offers',
    path: '#news_section',
    offset: '70',
  },  {
    label: 'Partner',
    path: '#ride_section',
    offset: '70',
  },
];
export const MENU_LEFT_ITEMS = [
  {
    label: 'Home',
    path: '#banner_section',
    offset: '70',
  },
   {
    label: 'Our Services',
    path: '#feature_section',
    offset: '70',
  },

  {
    label: 'About Us',
    path: '#fare_section',
    offset: '70',
  },
 
  {
    label: 'Offers',
    path: '#news_section',
    offset: '70',
  },  {
    label: 'Partner',
    path: '#ride_section',
    offset: '70',
  },
];
export const MENU_RIGHT_ITEMS = [
  {
    label: 'Login',
    path: '#',
    offset: '70',
  },
  {
    label: 'Sign Up',
    path: '#',
    offset: '70',
  },
];

export const Features = [
  {
    id: 1,
    img: `${Image1}`,
    title: 'Food',
    description:
      'Documents, accessories, packages and even gifts! Deliver them all within your city, in a jiffy!',
  },
  {
    id: 2,
    img: `${Image2}`,
    title: 'Fresh Meat',
    description:
      'All the Riders have been verified by us. Not random people with bikes that we don’t know.',
  },
  {
    id: 3,
    img: `${Image3}`,
    title: 'Grocery',
    description:
      'Order food from your favorite Place near you with the convenience of Godrive.',
  },  {
    id: 4,
    img: `${Image4}`,
    title: 'Fruits',
    description:
      'Order food from your favorite Place near you with the convenience of Godrive.',
  },  {
    id: 5,
    img: `${Image5}`,
    title: 'Vegetables',
    description:
      'Order food from your favorite Place near you with the convenience of Godrive.',
  }, {
    id: 6,
    img: `${Image6}`,
    title: 'Fresh Fish',
    description:
      'Order food from your favorite Place near you with the convenience of Godrive.',
  },
];
export const LatestNews = [
  {
    id: 1,
    img: `${Latest1}`,
    title: 'why dwizy for you',
    description:
      'Dwizy deliver food, meat, grocery, vegetables and fruits and other needs to your doors on time. Dwizy will make your daily life easy',
    buttonText: 'Learn More',
  },
  {
    id: 2,
    img: `${Latest2}`,
    title: 'Why we are using NEXT Js',
    description:
      'Yes!!,we are with technology. Dwizy trust technology,new era NEXT is the server side rendering library which increases the performance and usability in minimal speed of internet .',
    buttonText: 'Learn More',
  },
];
export const Testimonial = [
  {
    id: 1,
    name: 'Michal Corleone Jr.',
    designation: 'CEO of Invission Co.',
    comment:
      'Love to work with this designer in every our future project to ensure we are going to build best prototyping features. Impressed with master class support of the team and really look forward for the future. A true passionate team.',
    avatar_url: Client1,
    social_icon: 'flaticon-instagram',
  },
  {
    id: 2,
    name: 'Roman Ul Oman',
    designation: 'Co-founder of QatarDiaries',
    comment:
      'Impressed with master class support of the team and really look forward for the future. A true passionate team. Love to work with this designer in every our future project to ensure we are going to build best prototyping features.',
    avatar_url: Client2,
    social_icon: 'flaticon-twitter',
  },
];
export const menuWidget = [
  {
    id: 1,
    title: 'Company',
    menuItems: [
      {
        id: 1,
        url: '#',
        text: 'Support Center',
      },
      {
        id: 2,
        url: '#',
        text: 'Customer Support',
      },
      {
        id: 3,
        url: '#',
        text: 'About Us',
      },
      {
        id: 4,
        url: '#',
        text: 'Copyright',
      },
      // {
      //   id: 5,
      //   url: '#',
      //   text: 'Popular Campaign',
      // },
    ],
  },
  {
    id: 2,
    title: 'Our Information',
    menuItems: [
      {
        id: 1,
        url: '#',
        text: 'Return Policy',
      },
      {
        id: 2,
        url: '#',
        text: 'Privacy Policy',
      },
      {
        id: 3,
        url: '#',
        text: 'Terms & Conditions',
      },
      // {
      //   id: 4,
      //   url: '#',
      //   text: 'Site Map',
      // },
      // {
      //   id: 5,
      //   url: '#',
      //   text: 'Store Hours',
      // },
    ],
  },
  // {
  //   id: 3,
  //   title: 'My Account',
  //   menuItems: [
  //     {
  //       id: 1,
  //       url: '#',
  //       text: 'Press inquiries',
  //     },
  //     {
  //       id: 2,
  //       url: '#',
  //       text: 'Social media directories',
  //     },
  //     {
  //       id: 3,
  //       url: '#',
  //       text: 'Images & B-roll',
  //     },
  //     {
  //       id: 4,
  //       url: '#',
  //       text: 'Permissions',
  //     },
  //     {
  //       id: 5,
  //       url: '#',
  //       text: 'Speaker requests',
  //     },
  //   ],
  // },
];
export const Language_NAMES = [
  {
    label: 'English',
    value: 'eng',
  },
  {
    label: 'Chinese',
    value: 'chinese',
  },
  {
    label: 'Indian',
    value: 'indian',
  },
];

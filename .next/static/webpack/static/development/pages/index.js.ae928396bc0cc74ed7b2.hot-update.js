webpackHotUpdate("static\\development\\pages\\index.js",{

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/head */ "./node_modules/next-server/dist/lib/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var react_stickynode__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-stickynode */ "./node_modules/react-stickynode/index.js");
/* harmony import */ var react_stickynode__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(react_stickynode__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var styled_components__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! styled-components */ "./node_modules/styled-components/dist/styled-components.browser.esm.js");
/* harmony import */ var _common_src_theme_ride__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../common/src/theme/ride */ "./common/src/theme/ride/index.js");
/* harmony import */ var _common_src_assets_css_style__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../common/src/assets/css/style */ "./common/src/assets/css/style.js");
/* harmony import */ var _common_src_containers_Ride_ride_style__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../common/src/containers/Ride/ride.style */ "./common/src/containers/Ride/ride.style.js");
/* harmony import */ var _common_src_contexts_DrawerContext__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../common/src/contexts/DrawerContext */ "./common/src/contexts/DrawerContext.js");
/* harmony import */ var _common_src_containers_Ride_Navbar__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../common/src/containers/Ride/Navbar */ "./common/src/containers/Ride/Navbar/index.js");
/* harmony import */ var _common_src_containers_Ride_Banner__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../common/src/containers/Ride/Banner */ "./common/src/containers/Ride/Banner/index.js");
/* harmony import */ var _common_src_containers_Ride_RideOption__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../common/src/containers/Ride/RideOption */ "./common/src/containers/Ride/RideOption/index.js");
/* harmony import */ var _common_src_containers_Ride_LocationSelection__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../common/src/containers/Ride/LocationSelection */ "./common/src/containers/Ride/LocationSelection/index.js");
/* harmony import */ var _common_src_containers_Ride_Feature__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../common/src/containers/Ride/Feature */ "./common/src/containers/Ride/Feature/index.js");
/* harmony import */ var _common_src_containers_Ride_LatestNews__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../common/src/containers/Ride/LatestNews */ "./common/src/containers/Ride/LatestNews/index.js");
/* harmony import */ var _common_src_containers_Ride_HowItWorks__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../common/src/containers/Ride/HowItWorks */ "./common/src/containers/Ride/HowItWorks/index.js");
/* harmony import */ var _common_src_containers_Ride_TestimonialSection__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../common/src/containers/Ride/TestimonialSection */ "./common/src/containers/Ride/TestimonialSection/index.js");
/* harmony import */ var _common_src_containers_Ride_FeatureSlider__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../common/src/containers/Ride/FeatureSlider */ "./common/src/containers/Ride/FeatureSlider/index.js");
/* harmony import */ var _common_src_containers_Ride_Footer__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../common/src/containers/Ride/Footer */ "./common/src/containers/Ride/Footer/index.js");
var _jsxFileName = "D:\\Dwizy Repos\\dwizy-next\\pages\\index.js";


















/* harmony default export */ __webpack_exports__["default"] = (function () {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(styled_components__WEBPACK_IMPORTED_MODULE_3__["ThemeProvider"], {
    theme: _common_src_theme_ride__WEBPACK_IMPORTED_MODULE_4__["rideTheme"],
    __source: {
      fileName: _jsxFileName,
      lineNumber: 25
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react__WEBPACK_IMPORTED_MODULE_0__["Fragment"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 26
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_head__WEBPACK_IMPORTED_MODULE_1___default.a, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 27
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("title", {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 28
    },
    __self: this
  }, "Dwizy for you "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "theme-color",
    content: "#a52121",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 29
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("meta", {
    name: "Description",
    content: "Dwizy for you , order what you want near to you",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 30
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("link", {
    rel: "stylesheet",
    href: "https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.css",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 31
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("link", {
    href: "https://fonts.googleapis.com/css?family=Raleway",
    rel: "stylesheet",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 32
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("link", {
    rel: "stylesheet",
    href: "/static/base.css",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("link", {
    href: "https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Lato:300,400,700",
    rel: "stylesheet",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 35
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_src_assets_css_style__WEBPACK_IMPORTED_MODULE_5__["ResetCSS"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 40
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_src_containers_Ride_ride_style__WEBPACK_IMPORTED_MODULE_6__["GlobalStyle"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_src_containers_Ride_ride_style__WEBPACK_IMPORTED_MODULE_6__["ContentWrapper"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 42
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_stickynode__WEBPACK_IMPORTED_MODULE_2___default.a, {
    top: 0,
    innerZ: 9999,
    activeClass: "sticky-nav-active",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_src_contexts_DrawerContext__WEBPACK_IMPORTED_MODULE_7__["DrawerProvider"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 44
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_src_containers_Ride_Navbar__WEBPACK_IMPORTED_MODULE_8__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 45
    },
    __self: this
  }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_src_containers_Ride_Banner__WEBPACK_IMPORTED_MODULE_9__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 48
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_src_containers_Ride_Feature__WEBPACK_IMPORTED_MODULE_12__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 49
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_src_containers_Ride_LocationSelection__WEBPACK_IMPORTED_MODULE_11__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 50
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_src_containers_Ride_LatestNews__WEBPACK_IMPORTED_MODULE_13__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 52
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_common_src_containers_Ride_Footer__WEBPACK_IMPORTED_MODULE_17__["default"], {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 57
    },
    __self: this
  }))));
});

/***/ })

})
//# sourceMappingURL=index.js.ae928396bc0cc74ed7b2.hot-update.js.map
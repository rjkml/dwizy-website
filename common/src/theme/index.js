import { appTheme } from './app';
import { agencyTheme } from './agency';
import {ride} from './saasTwo';


export const theme = {
  appTheme,
  agencyTheme,
  ride
};

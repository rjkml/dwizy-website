import React, { Fragment } from 'react';
import Head from 'next/head';
import Sticky from 'react-stickynode';
import { ThemeProvider } from 'styled-components';
import { rideTheme } from '../common/src/theme/ride';
import { ResetCSS } from '../common/src/assets/css/style';
import {
  GlobalStyle,
  ContentWrapper,
} from '../common/src/containers/Ride/ride.style';
import { DrawerProvider } from '../common/src/contexts/DrawerContext';
import Navbar from '../common/src/containers/Ride/Navbar';
import Banner from '../common/src/containers/Ride/Banner';
import RideOption from '../common/src/containers/Ride/RideOption';
import LocationSection from '../common/src/containers/Ride/LocationSelection';
import FeatureSection from '../common/src/containers/Ride/Feature';
import LatestNewsSection from '../common/src/containers/Ride/LatestNews';
import HowItWorkSection from '../common/src/containers/Ride/HowItWorks';
import TestimonialSection from '../common/src/containers/Ride/TestimonialSection';
import FeatureSlider from '../common/src/containers/Ride/FeatureSlider';
import Footer from '../common/src/containers/Ride/Footer';

export default () => {
  return (
    <ThemeProvider theme={rideTheme}>
      <Fragment>
        <Head>
          <title>Dwizy for you </title>
          <meta name="theme-color" content="#a52121" />
          <meta name="Description" content="Dwizy for you , order what you want near to you" />
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.css" />

  <link rel="stylesheet" href="/static/base.css" />
          {/* Load google fonts */}
          <link
            href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Lato:300,400,700"
            rel="stylesheet"
          />
        </Head>
        <ResetCSS />
        <GlobalStyle />
        <ContentWrapper>
          <Sticky top={0} innerZ={9999} activeClass="sticky-nav-active">
            <DrawerProvider>
              <Navbar />
            </DrawerProvider>
          </Sticky>
          <Banner />
          <FeatureSection />
          <LocationSection />
          {/* <FeatureSlider />s */}
          <LatestNewsSection />
          {/* <HowItWorkSection />  */}
          
          {/* <RideOption /> */}
          {/* <TestimonialSection /> */}
          <Footer />
        </ContentWrapper>
      </Fragment>
    </ThemeProvider>
  );
};
